import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

var Windows = window.Windows;

const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
}));

const tiers = [
  {
    title: 'PC Gallery',
    description: ['Photos', 'Videos', 'Effects', 'Edit'],
    buttonText: 'Enjoy!',
    buttonVariant: 'contained',
  },
  {
    title: 'Studio Plus',
    description: [
      'Videos',
      'Photos',
      'Edit',
      'Music',
    ],
    buttonText: 'Enjoy!',
    buttonVariant: 'contained',
  },
  {
    title: 'Samsung Health',
    description: [
      'Lifestyle',
      'Health',
      'Help center access',
      'Support',
    ],
    buttonText: 'Enjoy!',
    buttonVariant: 'contained',
  },
];

const toastNotificationXmlTemplate =
  `<toast>
    <visual>
        <binding template="ToastGeneric">
            <text hint-maxLines="1"></text>
            <text></text>
        </binding>
    </visual>
</toast>`;

function showToastNotification(title, message) {
  const toastXml = new Windows.Data.Xml.Dom.XmlDocument();
  toastXml.loadXml(toastNotificationXmlTemplate);

  let textNodes = toastXml.getElementsByTagName('text');
  textNodes[0].innerText = title;
  textNodes[1].innerText = message;

  const toast = new Windows.UI.Notifications.ToastNotification(toastXml);
  Windows.UI.Notifications.ToastNotificationManager.createToastNotifier().show(toast);
}

function setNewTitle(newTitle) {
  Windows.UI.ViewManagement.ApplicationView.getForCurrentView().title = newTitle;
}

function showMessageDialog(title, content, options) {
  return new Promise((resolve, reject) => {
    var dialog = new Windows.UI.Popups.MessageDialog(content, title);
    if (options instanceof Array && options.length) {
      options.forEach((optionText, ix) => {
        dialog.commands.append(new Windows.UI.Popups.UICommand(
          optionText,
          () => resolve({ option: optionText, optionIndex: ix })));
      });
    }

    dialog.showAsync();
  });
}

function onShare(title, text, url) {
  const DataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager;

  const dataTransferManager = DataTransferManager.getForCurrentView();
  dataTransferManager.addEventListener("datarequested", (ev) => {
    const data = ev.request.data;

    data.properties.title = title;
    data.properties.url = url;
    data.setText(text);
  });

  DataTransferManager.showShareUI();

  return true;
}

function onClickHandle() {
  if (window.Windows) {
    //setNewTitle('NEW TITLE');
    //showToastNotification('Toast!', 'This is a Toast.');
    //showMessageDialog('PC Gallery', 'Do you want to open?', ['Yes', 'Cancel']);
    onShare('Share', 'Shared Message', 'http://localhost:3000');
  }
}

export default function Pricing() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      {/* Hero unit */}
      <Container maxWidth="sm" component="main" className={classes.heroContent}>
        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
          Samsung Gift
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {tiers.map((tier) => (
            // Enterprise card is full width at sm breakpoint
            <Grid item key={tier.title} xs={12} sm={tier.title === 'Samsung Health' ? 12 : 6} md={6}>
              <Card>
                <CardHeader
                  title={tier.title}
                  subheader={tier.subheader}
                  titleTypographyProps={{ align: 'center' }}
                  subheaderTypographyProps={{ align: 'center' }}
                  action={tier.title === 'Pro' ? <StarIcon /> : null}
                  className={classes.cardHeader}
                />
                <CardContent>
                  <ul>
                    {tier.description.map((line) => (
                      <Typography component="li" variant="subtitle1" align="center" key={line}>
                        {line}
                      </Typography>
                    ))}
                  </ul>
                </CardContent>
                <CardActions>
                  <Button onClick={onClickHandle} fullWidth variant={tier.buttonVariant} color="primary">
                    {tier.buttonText}
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
}
